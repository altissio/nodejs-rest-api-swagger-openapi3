'use strict';

const config = require('../env.json')[process.env.NODE_ENV || 'development'];
const security = require('../class/security.class');



/**
* @function auth
* @route /v1/auth
* @type POST
*/
module.exports.auth = function auth (req, res, next) {

  const sec = new security();
  const apiKey = sec.getApiKey();
  res.status(200).json({"apiKey":apiKey});

}