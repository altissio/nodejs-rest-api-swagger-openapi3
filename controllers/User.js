'use strict';


/**
* @function getUser
* @route /user/{userId}
* @type GET
*/
module.exports.getUser = function getUser (req, res, next, userId) {

  const user = {
    "id":userId,
    "firstname":"John",
    "name":"Doe"
  }
  res.status(200).json(user);

};


/**
* @function updateUser
* @route /user/{userId}
* @type PUT
*/
module.exports.updateUser = function updateUser (req, res, next, userId) {

  res.status(201).json({"message":"resource updated successfully"});

};


/**
* @function deleteUser
* @route /user/{userId}
* @type DELETE
*/
module.exports.deleteUser = function deleteUser (req, res, next, userId) {

  res.status(201).json({"message":"resource deleted successfully"});

};


/**
* @function addUser
* @route /user
* @type POST
*/
module.exports.addUser = function addUser (req, res, next) {

  res.status(201).json({"message":"resource added successfully"});

};

