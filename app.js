'use strict';

/****************************** BEGIN EDITABLE **************************************/

/**
* See env.json file
*/
process.env.NODE_ENV = 'devLocal';

/****************************** END OF EDITABLE **************************************/



/**
* dependencies
*/
const path = require('path');
const http = require('http');
const oas3Tools = require('oas3-tools');
const security = require('./class/security.class');
const sec = new security();

/**
* Environment config file
*/
process.env.NODE_CONFIG_DIR = __dirname+'/config';
const config = require('./env.json')[process.env.NODE_ENV||'production'];

/**
* SwaggerRouter configuration
*/
let options={
  routing: {
    controllers: path.join(__dirname, './controllers')
  },
  logging: {
    format: 'combined',
    errorLimit: 400
  },
  openApiValidator: {
    validateSecurity: {
      handlers: {
        ApiKeyAuth(req, scope) {
          return sec.checkApiKey(req)
        }
      }
    }
  }
};

const expressAppConfig=oas3Tools.expressAppConfig(path.join(__dirname, 'api/openapi.yaml'), options);
const app = expressAppConfig.getApp();


/**
* Initialize the Swagger middleware
*/
http.createServer(app).listen(config.port, function() {
  console.log("Your server is done!");
});
