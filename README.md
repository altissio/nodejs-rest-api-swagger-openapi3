## API REST
REST API with avec NodeJs, Swagger and OpenAPI 3

## Dependencies

* [NodeJs](https://nodejs.org/en/) - NodeJS Asynchronous event driven JavaScript runtime
* [oas3Tools](https://www.npmjs.com/package/oas3-tools) - Middleware for Open API 3
* [MySQL](https://www.npmjs.com/package/mysql) -  Driver for mysql db


## Requirements

* Node
* Git

## Common setup

Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/altissio/nodejs-rest-api-swagger-openapi3
cd the-example-app.nodejs
```

```bash
npm install
```

## Start API

To start the api server, run the following

```bash
node app.js
```


## Open Swagger docs

```bash
http://localhost:8080/docs/#/
```