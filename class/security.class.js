const config = require('../env.json')[process.env.NODE_ENV || 'development'];
const jwt = require('jsonwebtoken');

module.exports = class Security {

  /**
  * @constructor
  */
  constructor( ) {}

  /**
  * @function getApiKey
  * @return {string} token
  */
  getApiKey(){
    let token = jwt.sign({ "param1": "param1value", "param2": "param2value" }, config.jwt.secret, {
      expiresIn: 3600 // expires in 1 hours
    });
    return token;
  }

  /**
  * @function checkApiKey
  * @param {json} req
  * @return {boolean}
  */
  checkApiKey(req){
    const token = req.headers.apikey;
    if (!token){
      token = req.query.token;
      if(!token){
        return false;
      }
    }
    let decoded;
    try {
      decoded = jwt.verify(token, config.jwt.secret);
    } catch(err) {
      throw (err);
    }

    return true;
  }
  

}
